
This plugin will add actions on tabs and project for:
<ul>
  <li>Close projects' tabs</li>
  <li>Close other projects' tabs</li>
  <li>Close projects' unchanged tabs</li>
  <li>Close projects' and sub projects' tabs</li>
</ul>
The plugin will also add a window that lists all open tabs as table and project tree table. The view has support for <strong>text and regex search and column filter and sorting</strong> to help narrowing down the list of open tabs.