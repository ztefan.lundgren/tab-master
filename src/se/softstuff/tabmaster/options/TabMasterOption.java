package se.softstuff.tabmaster.options;

import java.util.prefs.BackingStoreException;
import java.util.prefs.NodeChangeListener;
import java.util.prefs.PreferenceChangeListener;
import java.util.prefs.Preferences;
import org.openide.util.Exceptions;
import org.openide.util.NbPreferences;


public class TabMasterOption {
    private static TabMasterOption INSTANCE;
    
    public static TabMasterOption getDefault(){
        if(INSTANCE == null){
            synchronized(TabMasterOption.class){
                if(INSTANCE == null){
                    INSTANCE = new TabMasterOption();
                }
            }
        }
        return INSTANCE;
    }
    
    public boolean isPreviewEnable(){
        return getPref().getBoolean("preview", false);        
    }
    
    public void setPreviewEnable(boolean value) {
        getPref().putBoolean("preview", value);
    }
    
    
    
    public void addNodeChangeListener(NodeChangeListener ncl) {
        getPref().addNodeChangeListener(ncl);
    }
    
    public void removeNodeChangeListener(NodeChangeListener ncl) {
        getPref().removeNodeChangeListener(ncl);
    }
    
     public void addPreferenceChangeListener(PreferenceChangeListener pcl) {
         getPref().addPreferenceChangeListener(pcl);         
     }
     
     public void removePreferenceChangeListener( PreferenceChangeListener pcl) {
         getPref().removePreferenceChangeListener(pcl);
     }
    
    
    private Preferences getPref() {
        return NbPreferences.forModule(getClass());
    }

    public void save() {
        try {
            getPref().sync();
        } catch (BackingStoreException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
