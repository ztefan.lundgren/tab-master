package se.softstuff.tabmaster.prevew;

import java.beans.PropertyChangeEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.api.editor.mimelookup.MimeLookup;
import org.netbeans.api.editor.mimelookup.MimePath;
import org.openide.loaders.DataFolder;
import org.openide.loaders.DataObject;
import org.openide.modules.OnStart;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;
import org.openide.windows.CloneableOpenSupport;
import org.openide.windows.CloneableTopComponent;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import se.softstuff.tabmaster.options.TabMasterOption;

@OnStart
public class FilePreviewManager implements Runnable {

    private static final Logger LOG = Logger.getLogger(FilePreviewManager.class.getName());
    public static final String TAB_MASTER_PREVIEW = "TabMasterPreview";

    private DataObject currentPreviewNode;

    @Override
    public void run() {
        TopComponent.getRegistry().addPropertyChangeListener((PropertyChangeEvent evt) -> {
            if (isPreviewEvent(evt)) {
                for (Node node : (Node[]) evt.getNewValue()) {
                    DataObject data = node.getLookup().lookup(DataObject.class);
                    if (isValidForPreview(data)) {
                        currentPreviewNode = data;
                        previewFile(data);
                    }
                }
            }
        });
    }

    private boolean isPreviewEvent(PropertyChangeEvent evt) {
        return evt.getPropertyName().equals(TopComponent.Registry.PROP_ACTIVATED_NODES)
                && isProjectExplorerActive()
                && evt.getNewValue() != null
                && evt.getNewValue() instanceof Node[];
    }

    private boolean isValidForPreview(DataObject data) {
        return data != null
                && !(data instanceof DataFolder) 
                && !data.equals(currentPreviewNode)
                && TabMasterOption.getDefault().isPreviewEnable();
    }

    private boolean isProjectExplorerActive() {
        TopComponent activated = WindowManager.getDefault().getRegistry().getActivated();
        if(activated==null){
            return false;
        }
        String activeTopComponentID = WindowManager.getDefault().findTopComponentID(activated);
        return activeTopComponentID.equalsIgnoreCase("projectTabLogical_tc") || activeTopComponentID.equalsIgnoreCase("projectTab_tc");
    }

    private CloneableTopComponent createCloneableTopComponent(Class<?> aClass, Object obj) {

        if (aClass == null) {
            return null;
        }
        try {
            Method method = aClass.getDeclaredMethod("createCloneableTopComponent");
            method.setAccessible(true);
            Object invoke = method.invoke(obj);
            CloneableTopComponent tc = (CloneableTopComponent) invoke;
            return tc;

        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException | SecurityException ex) {
            Exceptions.printStackTrace(ex);
        }
        return createCloneableTopComponent(aClass.getSuperclass(), obj);
    }

    private void previewFile(DataObject data) {

        Runnable openTask = () -> {
            cloasAllPreviewTabs();

            String mimeType = data.getPrimaryFile().getMIMEType();
            MimePath mimePath = MimePath.get(mimeType);
            final Lookup lkp = MimeLookup.getLookup(mimePath);
            CloneableOpenSupport cp = data.getLookup().lookup(CloneableOpenSupport.class);
            if (cp != null) {
                CloneableTopComponent editor = createCloneableTopComponent(CloneableOpenSupport.class, cp);

                makeToPreviewTab(editor);
                editor.openAtTabPosition(0);
                editor.requestActive();

                Collection<? extends Node> tc_nodes = editor.getLookup().lookupAll(Node.class);
                tc_nodes.size();
            }
        };

        if (SwingUtilities.isEventDispatchThread()) {
            openTask.run();
        } else {
            SwingUtilities.invokeLater(openTask);
        }
    }

    private void cloasAllPreviewTabs() {
        TopComponent.getRegistry().getOpened().stream()
                .filter((tc) -> tc.getClientProperty(TAB_MASTER_PREVIEW) != null)
                .forEach((tc) -> tc.close());
    }

    private void makeToPreviewTab(CloneableTopComponent editor) {
        editor.putClientProperty(TAB_MASTER_PREVIEW, "Japp");
    }

}
