package se.softstuff.tabmaster.prevew;

import java.awt.Component;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import org.netbeans.core.multitabs.TabDecorator;
import org.netbeans.swing.tabcontrol.TabData;
import org.openide.util.ImageUtilities;
import org.openide.util.lookup.ServiceProvider;
import org.openide.windows.TopComponent;


@ServiceProvider(service=TabDecorator.class, position = 1000)
public class PreviewTabDecorator extends TabDecorator{

    public static final ImageIcon IMG_VIEW = ImageUtilities.loadImageIcon("se/softstuff/tabmaster/timeline/eye.png", false);
    
    @Override
    public Icon getIcon(org.netbeans.swing.tabcontrol.TabData tab) {
        if(isPreviewTab(tab)){
            return IMG_VIEW; 
        }
        return null;
    }

    private boolean isPreviewTab(TabData tab) {
        return tab.getUserObject() instanceof TopComponent 
                && ((TopComponent)tab.getUserObject()).getClientProperty(FilePreviewManager.TAB_MASTER_PREVIEW) != null;
    }
}
