package se.softstuff.tabmaster.prevew;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.awt.StatusDisplayer;
import org.openide.util.NbBundle;
import org.openide.util.NbBundle.Messages;
import se.softstuff.tabmaster.options.TabMasterOption;

@ActionID(
        category = "Tab",
        id = "se.softstuff.tabmaster.prevew.prevewTogglePreviewAction"
)
@ActionRegistration(
        displayName = "#CTL_TogglePreviewAction"
)
@ActionReference(path = "Shortcuts", name = "DO-P")
@Messages({"CTL_TogglePreviewAction=Toggle Preview",
    "CTL_TogglePreviewAction.status.true=Tab preview was turned on", 
    "CTL_TogglePreviewAction.status.false=Tab preview was turned off"})
public final class TogglePreviewAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {
        TabMasterOption option = TabMasterOption.getDefault();
        option.setPreviewEnable(!option.isPreviewEnable());
        option.save();
        StatusDisplayer.getDefault().setStatusText(NbBundle.getMessage(getClass(), "CTL_TogglePreviewAction.status."+option.isPreviewEnable()));
    }
}
