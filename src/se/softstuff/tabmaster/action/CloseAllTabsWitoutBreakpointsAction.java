package se.softstuff.tabmaster.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.api.debugger.Properties;
import org.netbeans.api.debugger.jpda.LineBreakpoint;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.util.Exceptions;
import se.softstuff.tabmaster.EditorUtils;

@ActionID(
        category = "Tab",
        id = "se.softstuff.tabmaster.action.CloseAllTabsWitoutBreakpointsAction"
)
@ActionRegistration(
        displayName = "#CTL_CloseAllTabsWitoutBreakpointsAction"
)
@ActionReferences({
    @ActionReference(path = "Editors/TabActions", name = "tm-editor-CloseAllTabsWitoutBreakpointsAction", position = 20)
})
public class CloseAllTabsWitoutBreakpointsAction implements ActionListener {

    @Override
    public void actionPerformed(ActionEvent e) {

        Properties debugger = Properties.getDefault().getProperties("debugger");
        Properties propBreakP = debugger.getProperties(DebuggerManager.PROP_BREAKPOINTS);
        Breakpoint[] breakpoints = (Breakpoint[]) propBreakP.getArray("jpda", new Breakpoint[0]);

        Set<FileObject> filesWithBreakpoints = Arrays.asList(breakpoints).stream()
                .map(bp -> {
                    if (bp instanceof LineBreakpoint) {
                        LineBreakpoint lb = (LineBreakpoint) bp;
                        try {
                            URL url = new URL(lb.getURL());
                            return URLMapper.findFileObject(url);

                        } catch (MalformedURLException ex) {
                            Exceptions.printStackTrace(ex);
                        }
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());

        EditorUtils.getEditorTabs().stream()
                .filter(tc -> !filesWithBreakpoints.contains(EditorUtils.getPrimaryFileFor(tc)))
                .forEach(tc -> {
                    tc.close();
                });
    }
}
