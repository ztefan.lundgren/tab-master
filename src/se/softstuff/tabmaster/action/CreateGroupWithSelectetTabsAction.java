//package se.softstuff.tabmaster.action;
//
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.lang.reflect.InvocationTargetException;
//import java.lang.reflect.Method;
//import java.util.List;
//import java.util.Set;
//import org.openide.awt.ActionID;
//import org.openide.awt.ActionReference;
//import org.openide.awt.ActionRegistration;
//import org.openide.util.Exceptions;
//import org.openide.windows.Mode;
//import org.openide.windows.TopComponent;
//import org.openide.windows.TopComponentGroup;
//import org.openide.windows.WindowManager;
//import se.softstuff.tabmaster.override.MyDocumentGroup;
//import se.softstuff.tabmaster.override.MyGroupsManager;
//import se.softstuff.tabmaster.node.Tab;
//
//@ActionID(
//        category = "Tab",
//        id = "se.softstuff.tabmaster.action.CreateGroupWithSelectetTabsAction"
//)
//@ActionRegistration(
//        displayName = "#CreateGroup"
//)
//@ActionReference(path = "Tab/Actions", name = "createGroup", position = 1002)
//public class CreateGroupWithSelectetTabsAction implements ActionListener {
//
//    private final List<Tab> context;
//
//    public CreateGroupWithSelectetTabsAction(List<Tab> tabs) {
//        this.context = tabs;
//    }
//
//    @Override
//    public void actionPerformed(ActionEvent e) {
//        WindowManager wm = WindowManager.getDefault();
//        Set<? extends Mode> modes = wm.getModes();
//        
//        
//        MyGroupsManager groupsManager = MyGroupsManager.getDefault();
//        List<MyDocumentGroup> groups = groupsManager.getGroups();
//        groupsManager.addGroup("test");
//        List<MyDocumentGroup> groups2 = groupsManager.getGroups();
//        MyDocumentGroup first = groups2.get(0);
//        String displayName = first.getDisplayName();
//        String name = first.getName();
//        
//        groupsManager.openGroup(first);
//        
//        
//        TopComponentGroup group = wm.findTopComponentGroup("");
//        
//        Set<TopComponentGroup> topComponentGroups = getTopComponentGroups();
//        topComponentGroups.forEach(tcg->{
//            System.out.println(tcg);
//        });
//        modes.size();
////        group.open();
////        NewTabGroupAction
////        Kolla om man via reflection når dessa
////                
////                TopComponentGroupImpl
////                open
////                        Set<TopComponent> getTopComponents()
////                                get name
////                        
////                WindowManagerImpl
////                public void addTopComponentGroup(TopComponentGroupImpl tcGroup)
////
////    public void removeTopComponentGroup(TopComponentGroupImpl tcGroup)
////
////    public Set<TopComponentGroupImpl> getTopComponentGroups()
////    
//    }
//    
//    private Set<TopComponentGroup> getTopComponentGroups(){
//        
//        try {
//            WindowManager wm = WindowManager.getDefault();
//            
//            Method getTopComponentGroups = wm.getClass().getMethod("getTopComponentGroups");
//            Object invoke = getTopComponentGroups.invoke(wm);
//            return (Set<TopComponentGroup>)invoke;
//        } catch (IllegalAccessException ex) {
//            Exceptions.printStackTrace(ex);
//        } catch (IllegalArgumentException ex) {
//            Exceptions.printStackTrace(ex);
//        } catch (InvocationTargetException ex) {
//            Exceptions.printStackTrace(ex);
//        } catch (NoSuchMethodException ex) {
//            Exceptions.printStackTrace(ex);
//        } catch (SecurityException ex) {
//            Exceptions.printStackTrace(ex);
//        }
//        return null;
//    }
//}
