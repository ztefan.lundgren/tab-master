package se.softstuff.tabmaster.action;

import java.awt.event.ActionEvent;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import javax.swing.AbstractAction;
import javax.swing.Action;
import static javax.swing.Action.NAME;
import static javax.swing.Action.SMALL_ICON;
import org.netbeans.api.annotations.common.StaticResource;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionReferences;
import org.openide.awt.ActionRegistration;
import org.openide.loaders.DataObject;
import org.openide.util.ContextAwareAction;
import org.openide.util.ImageUtilities;
import org.openide.util.Lookup;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import se.softstuff.tabmaster.EditorUtils;

@ActionID(
        category = "Project",
        id = "se.softstuff.tabmaster.action.CloseProjectsTabsAction"
)
@ActionRegistration(
        displayName = "#CTL_CloseProjectsTabsAction",
        lazy = false
)
@ActionReferences({
    @ActionReference(path = "Projects/Actions", name = "tm-proj-CloseProjectsTabsAction", position = 10)
    ,@ActionReference(path = "Editors/TabActions", name = "tm-editor-CloseProjectsTabsAction", position = 10)
})
public final class CloseProjectsTabsAction extends AbstractAction implements ContextAwareAction {

    @StaticResource
    private static final String ICON = "se/softstuff/tabmaster/action/application_delete.png";
    private static final long serialVersionUID = 1L;
    private Lookup context;

    public CloseProjectsTabsAction() {
        putValue(SMALL_ICON, ImageUtilities.loadImageIcon(ICON, false));
        putValue(NAME, NbBundle.getMessage(getClass(), "CTL_CloseProjectsTabsAction"));
    }

    @Override
    public Action createContextAwareInstance(Lookup context) {
        this.context = context;
        return this;
    }

    @Override
    public boolean isEnabled() {
        Collection<? extends Project> projList = context.lookupAll(Project.class);
        TopComponent tc = context.lookup(TopComponent.class);
        DataObject dobj = context.lookup(DataObject.class);

        return dobj != null || tc != null || (projList != null && !projList.isEmpty());
    }

    @Override
    public void actionPerformed(ActionEvent ev) {

        Set<Project> projects = collectProjects();
        Collection<TopComponent> projectTabs = EditorUtils.getProjectsTabs(projects, false);
        cloasTabs(projectTabs);
    }

    private Set<Project> collectProjects() {
        final Collection<? extends Project> selectedProjects = context.lookupAll(Project.class);
        DataObject dataObj = context.lookup(DataObject.class);
        Set<Project> projects = new HashSet<>(selectedProjects);
        if (dataObj != null) {
            Project currentProject = FileOwnerQuery.getOwner(dataObj.getPrimaryFile());
            projects.add(currentProject);
        }
        return projects;
    }

    private void cloasTabs(Collection<? extends TopComponent> tabs) {
        tabs.forEach((tab) -> {
            tab.close();
        });
    }

}
