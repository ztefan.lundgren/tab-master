package se.softstuff.tabmaster.finder.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import se.softstuff.tabmaster.finder.Tab;

@ActionID(
        category = "Tab",
        id = "se.softstuff.tabmaster.action.CloseSelectetTabsAction"
)
@ActionRegistration(
        displayName = "#Close"
)
@ActionReference(path = "Tab/Actions", name = "closeTabs", position = 1000)
public class CloseSelectetTabsAction implements ActionListener {

    private final List<Tab> context;

    public CloseSelectetTabsAction(List<Tab> tabs) {
        this.context = tabs;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        context.forEach((tab) -> {
            tab.getTc().close();
        });
    }
}
