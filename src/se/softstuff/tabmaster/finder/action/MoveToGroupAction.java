package se.softstuff.tabmaster.finder.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;
import javax.swing.SwingUtilities;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import se.softstuff.tabmaster.finder.Tab;
import se.softstuff.tabmaster.override.MyWindowManager;

@ActionID(
        category = "Tab",
        id = "se.softstuff.tabmaster.action.MoveToGroupAction"
)
@ActionRegistration(
        displayName = "#CTL_MoveToGroupAction"
)
@ActionReference(path = "Tab/Actions", name = "addToGroup", position = 1002)
public final class MoveToGroupAction implements ActionListener {

    private final LinkedList<Tab> context;

    public MoveToGroupAction(List<Tab> context) {
        this.context = new LinkedList<>(context);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (context.isEmpty()) {
            return;
        }
        Runnable task = () -> {
            WindowManager wm = WindowManager.getDefault();
            TopComponent firstTc = context.pollFirst().getTc();
            MyWindowManager.getDefault().newTabGroup(firstTc);

            Mode newMode = wm.findMode(firstTc);
            context.forEach(tab -> {
                newMode.dockInto(tab.getTc());
                tab.getTc().setVisible(true);
            }
            );
        };

        if (SwingUtilities.isEventDispatchThread()) {
            task.run();
        } else {
            SwingUtilities.invokeLater(task);
        }
    }
}
