package se.softstuff.tabmaster.finder.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Optional;
import javax.swing.Action;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.ContextAwareAction;
import se.softstuff.tabmaster.finder.Tab;

@ActionID(
        category = "Tab",
        id = "se.softstuff.tabmaster.action.OpenSelectedTabInExplorerAction"
)
@ActionRegistration(
        displayName = "#CTL_OpenSelectedTabInExplorerAction"
)
@ActionReference(path = "Tab/Actions", name = "openTabInExplorer", position = 2)
public class OpenSelectedTabInExplorerAction implements ActionListener {

    private final Tab context;

    public OpenSelectedTabInExplorerAction(Tab tab) {
        this.context = tab;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Optional<Action> opt = Arrays.asList(context.getTc().getActions()).stream()
                .filter(action -> action != null && action.getClass().getName().equals("org.netbeans.modules.project.ui.actions.SelectNodeAction"))
                .findFirst();
        if (opt.isPresent()) {
            ContextAwareAction caa = (ContextAwareAction) opt.get();
            caa.createContextAwareInstance(context.getTc().getLookup()).actionPerformed(e);
        }
    }
}
