package se.softstuff.tabmaster.finder.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import se.softstuff.tabmaster.finder.Tab;

@ActionID(
        category = "Tab",
        id = "se.softstuff.tabmaster.action.OpenSelectedTabAction"
)
@ActionRegistration(
        displayName = "#CTL_OpenSelectedTabAction"
)
@ActionReference(path = "Tab/Actions", name = "openTab", position = 1)
public class OpenSelectedTabAction implements ActionListener {

    private final Tab context;

    public OpenSelectedTabAction(Tab tab) {
        this.context = tab;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        context.getTc().requestActive();
    }
}
