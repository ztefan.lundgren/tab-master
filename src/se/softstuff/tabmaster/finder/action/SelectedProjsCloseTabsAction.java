package se.softstuff.tabmaster.finder.action;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import se.softstuff.tabmaster.finder.Proj;

@ActionID(
        category = "Tab",
        id = "se.softstuff.tabmaster.action.SelectedProjsCloseTabsAction"
)
@ActionRegistration(
        displayName = "#CTL_CloseProjectsTabsAction"
)
@ActionReference(path = "TabProj/Actions", name = "closeProjsTabs", position = 1)
public class SelectedProjsCloseTabsAction implements ActionListener {

    private final List<Proj> context;

    public SelectedProjsCloseTabsAction(List<Proj> context) {
        this.context = context;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        context.forEach((proj) -> {
            proj.getTabs().forEach((tab) -> {
                tab.getTc().close();
            });
        });
    }
}
