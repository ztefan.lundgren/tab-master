package se.softstuff.tabmaster.finder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import org.netbeans.api.project.Project;
import org.openide.windows.TopComponent;

public class Proj {

    String name;
    Project project;
    List<Tab> tabs;

    public Proj() {
    }

    public Proj(String name, Tab... tab) {
        this.name = name;
        this.tabs = tab != null ? Arrays.asList(tab) : new ArrayList<>();
    }

    public Proj(Project proj, Collection<TopComponent> tabs) {
        this.name = proj != null ? proj.getProjectDirectory().getName() : "Other";
        this.project = proj;
        this.tabs = tabs.stream().map((tc) -> new Tab(project, tc)).collect(Collectors.toList());
    }

    public Project getProject() {
        return project;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Tab> getTabs() {
        return tabs;
    }

}
