package se.softstuff.tabmaster.finder;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import org.netbeans.api.project.ui.OpenProjects;
import org.netbeans.api.settings.ConvertAsProperties;
import org.netbeans.swing.etable.QuickFilter;
import org.netbeans.swing.outline.Outline;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeMemberEvent;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import se.softstuff.tabmaster.EditorUtils;
import se.softstuff.tabmaster.finder.node.ProjNode;
import se.softstuff.tabmaster.finder.node.ProjNodeFactory;
import se.softstuff.tabmaster.finder.node.TabNode;

@ConvertAsProperties(
        dtd = "-//se.softstuff.tabmaster//TabFinder//EN",
        autostore = false
)
@TopComponent.Description(
        preferredID = "TabFinderTopComponent",
        iconBase = "/se/softstuff/tabmaster/tab_go.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "bottomSlidingSide", openAtStartup = true)
@ActionID(category = "Window", id = "se.softstuff.tabmaster.TabFinderTopComponent")
@ActionReference(path = "Menu/Window", position = 995)
@TopComponent.OpenActionRegistration(
        displayName = "#CTL_TabFinderAction",
        preferredID = "TabFinderTopComponent"
)
public final class TabFinderTopComponent extends TopComponent implements ExplorerManager.Provider {

    private static final Logger LOG = Logger.getLogger(TabFinderTopComponent.class.getName());

    private final ExplorerManager em;
    private ProjNodeFactory nodeFactory;

    private final DocumentListener filterUpdateListener = new DocumentListener() {
        @Override
        public void changedUpdate(DocumentEvent e) {
            onFilterUpdate();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            onFilterUpdate();
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
            onFilterUpdate();
        }
    };

    PropertyChangeListener projectLoadListener = new PropertyChangeListener() {
        private final List<String> refreshEvents = Arrays.asList("tcOpened", "tcClosed", OpenProjects.PROPERTY_OPEN_PROJECTS);

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            LOG.log(Level.FINE, "OpenProjects event {0}", evt.getPropertyName());
            if (refreshEvents.contains(evt.getPropertyName())) {
                refreshNodesWhenProjectsIsDoneLoading();
            }
        }

        private void refreshNodesWhenProjectsIsDoneLoading() {
            new SwingWorker<Void, Void>() {
                @Override
                protected Void doInBackground() throws Exception {
                    EditorUtils.waitForProjectToLoad();
                    return null;
                }

                @Override
                protected void done() {
                    try {
                        get();
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                        Exceptions.printStackTrace(ex);
                    } catch (ExecutionException ex) {
                        Exceptions.printStackTrace(ex);
                    }
                    nodeFactory.refresh();
                }
            }.execute();
        }
    };
    
    private final PropertyChangeListener lastAccessMnitor=  new PropertyChangeListener() {
        private TopComponent lastTc;
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            if (isTCActivatedEvent(evt)) {
                SwingUtilities.invokeLater( ()->{
                    TopComponent tc = (TopComponent) evt.getNewValue();
                    if (WindowManager.getDefault().isEditorTopComponent(tc) && !tc.equals(lastTc)) {
                        tc.putClientProperty(EditorUtils.TM_LAST_ACCESS, LocalDateTime.now());
                        Integer frequency = (Integer)tc.getClientProperty(EditorUtils.TM_FREQUENCY);
                        if(frequency == null){
                            frequency = 0;
                        } 
                        
                        frequency = frequency +1;
                        
                        tc.putClientProperty(EditorUtils.TM_FREQUENCY, frequency);
                        LOG.info("TC " + tc.getName() + " evt:"+evt.getPropertyName()+", set last access now, frequency: "+frequency.intValue());
                        lastTc = tc;
                    }
                });
            }
        }
        
        private boolean isTCActivatedEvent(PropertyChangeEvent evt) {
            return evt.getPropertyName().equals("activated") && evt.getNewValue() != null && evt.getNewValue() instanceof TopComponent;
        }
    };

    public TabFinderTopComponent() {
        initComponents();
        setName(NbBundle.getMessage(getClass(), "CTL_TabFinderTopComponent"));
        setToolTipText(NbBundle.getMessage(getClass(), "HINT_TabFinderTopComponent"));

        filterTextField.getDocument().addDocumentListener(filterUpdateListener);

        view.setPropertyColumns(
                "projectName", NbBundle.getMessage(getClass(), "Project"),
                "timeSinceLastAccess", NbBundle.getMessage(getClass(), "Accessed"),
                "lastAccessStr", NbBundle.getMessage(getClass(), "LastAccess"),
                "frequencyStr", NbBundle.getMessage(getClass(), "Frequency"),
                "editedStr", NbBundle.getMessage(getClass(), "Edited"),
                "breakpointStr", NbBundle.getMessage(getClass(), "HasBreakpoint"),
                "context", NbBundle.getMessage(getClass(), "Context")
        );

        Outline outline = view.getOutline();
        outline.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        outline.setQuickFilter(0, new NameQuickFilter());
        outline.setRootVisible(false);
        
        em = new ExplorerManager();

        associateLookup(ExplorerUtils.createLookup(em, getActionMap()));

        nodeFactory = new ProjNodeFactory();
        Node rootNode = new AbstractNode(Children.create(nodeFactory, false));
        rootNode.addNodeListener(new ExpandChildNodeAdapter());
        em.setRootContext(rootNode);

        em.addVetoableChangeListener((PropertyChangeEvent evt) -> {
            if (evt.getPropertyName().equals(ExplorerManager.PROP_SELECTED_NODES)) {
                Node[] selection = (Node[]) evt.getNewValue();
                validateNodeSelection(selection);
            }
        });
        
        TopComponent.getRegistry().addPropertyChangeListener(lastAccessMnitor);
    }

    @Override
    public void componentOpened() {
        loadNodes();
    }

    @Override
    public void componentClosed() {
        OpenProjects.getDefault().removePropertyChangeListener(projectLoadListener);
        TopComponent.getRegistry().removePropertyChangeListener(projectLoadListener);
    }
    
    private void loadNodes() {
        new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                EditorUtils.waitForProjectToLoad();
                return null;
            }

            @Override
            protected void done() {
                OpenProjects.getDefault().addPropertyChangeListener(projectLoadListener);
                TopComponent.getRegistry().addPropertyChangeListener(projectLoadListener);
                nodeFactory.refresh();
            }

        }.execute();
    }

    private void useTreeTableLayout() {
        nodeFactory.setAddProjectNode(true);
    }

    private void useTableLayout() {
        nodeFactory.setAddProjectNode(false);
    }

    private void onFilterUpdate() {
        nodeFactory.refresh();
    }

    private String getFilter() {
        return filterTextField.getText().trim();
    }

    private boolean hasFilter() {
        return getFilter() != null && !getFilter().isEmpty();
    }

    private boolean isFilterRegexp() {
        return regExpFilterCheckBox.isSelected();
    }

    private boolean doFilter(String fileName) {
        if (hasFilter()) {
            if (isFilterRegexp()) {
                try {
                    Pattern regexPattern = Pattern.compile(getFilter());
                    Matcher regex = regexPattern.matcher(fileName);
                    return regex.matches();
                } catch (java.util.regex.PatternSyntaxException ex) {
                    return true;
                }
            } else {
                return fileName.toLowerCase().contains(getFilter().toLowerCase());
            }
        }
        return true;
    }

    @Override
    public ExplorerManager getExplorerManager() {
        return em;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        topPanel = new javax.swing.JPanel();
        filterLabel = new javax.swing.JLabel();
        filterTextField = new javax.swing.JTextField();
        selectLabel = new javax.swing.JLabel();
        selectAllButton = new javax.swing.JButton();
        selectInvertButton = new javax.swing.JButton();
        treeButton = new javax.swing.JToggleButton();
        tableButton = new javax.swing.JToggleButton();
        regExpFilterCheckBox = new javax.swing.JCheckBox();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        view = new org.openide.explorer.view.OutlineView();

        setLayout(new java.awt.BorderLayout());

        topPanel.setPreferredSize(new java.awt.Dimension(723, 25));

        org.openide.awt.Mnemonics.setLocalizedText(filterLabel, org.openide.util.NbBundle.getMessage(TabFinderTopComponent.class, "TabFinderTopComponent.filterLabel.text")); // NOI18N

        filterTextField.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));

        org.openide.awt.Mnemonics.setLocalizedText(selectLabel, org.openide.util.NbBundle.getMessage(TabFinderTopComponent.class, "TabFinderTopComponent.selectLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(selectAllButton, org.openide.util.NbBundle.getMessage(TabFinderTopComponent.class, "TabFinderTopComponent.selectAllButton.text")); // NOI18N
        selectAllButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectAllButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(selectInvertButton, org.openide.util.NbBundle.getMessage(TabFinderTopComponent.class, "TabFinderTopComponent.selectInvertButton.text")); // NOI18N
        selectInvertButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectInvertButtonActionPerformed(evt);
            }
        });

        buttonGroup1.add(treeButton);
        treeButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/se/softstuff/tabmaster/logical_view.png"))); // NOI18N
        treeButton.setToolTipText(org.openide.util.NbBundle.getMessage(TabFinderTopComponent.class, "TabFinderTopComponent.treeButton.toolTipText")); // NOI18N
        treeButton.setFocusable(false);
        treeButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        treeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                treeButtonActionPerformed(evt);
            }
        });

        buttonGroup1.add(tableButton);
        tableButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/se/softstuff/tabmaster/file_view.png"))); // NOI18N
        tableButton.setSelected(true);
        tableButton.setToolTipText(org.openide.util.NbBundle.getMessage(TabFinderTopComponent.class, "TabFinderTopComponent.tableButton.toolTipText")); // NOI18N
        tableButton.setFocusable(false);
        tableButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        tableButton.setInheritsPopupMenu(true);
        tableButton.setMargin(new java.awt.Insets(2, 2, 2, 2));
        tableButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tableButtonActionPerformed(evt);
            }
        });

        org.openide.awt.Mnemonics.setLocalizedText(regExpFilterCheckBox, org.openide.util.NbBundle.getMessage(TabFinderTopComponent.class, "TabFinderTopComponent.regExpFilterCheckBox.text")); // NOI18N

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout topPanelLayout = new javax.swing.GroupLayout(topPanel);
        topPanel.setLayout(topPanelLayout);
        topPanelLayout.setHorizontalGroup(
            topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topPanelLayout.createSequentialGroup()
                .addComponent(tableButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(treeButton, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filterLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(filterTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 257, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(regExpFilterCheckBox)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(selectLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(selectAllButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(selectInvertButton)
                .addContainerGap(58, Short.MAX_VALUE))
        );

        topPanelLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {selectAllButton, selectInvertButton});

        topPanelLayout.setVerticalGroup(
            topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topPanelLayout.createSequentialGroup()
                .addGroup(topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(filterTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(selectLabel)
                        .addComponent(selectAllButton)
                        .addComponent(selectInvertButton)
                        .addComponent(filterLabel)
                        .addComponent(regExpFilterCheckBox))
                    .addComponent(tableButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(treeButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jSeparator1))
                .addContainerGap())
        );

        add(topPanel, java.awt.BorderLayout.PAGE_START);
        add(view, java.awt.BorderLayout.CENTER);
    }// </editor-fold>//GEN-END:initComponents

    private void selectAllButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectAllButtonActionPerformed
        try {
            List<Node> allChildren = getAllNodes();
            em.setSelectedNodes(allChildren.toArray(new Node[0]));
        } catch (PropertyVetoException ex) {
            Exceptions.printStackTrace(ex);
        }
    }//GEN-LAST:event_selectAllButtonActionPerformed

    private void selectInvertButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectInvertButtonActionPerformed
        try {

            List<Node> selection = Arrays.asList(em.getSelectedNodes());
            List<Node> nodes = getAllNodes();
            nodes.removeAll(selection);
            em.setSelectedNodes(nodes.toArray(new Node[0]));
        } catch (PropertyVetoException ex) {
            Exceptions.printStackTrace(ex);
        }
    }//GEN-LAST:event_selectInvertButtonActionPerformed

    private void tableButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tableButtonActionPerformed
        useTableLayout();
    }//GEN-LAST:event_tableButtonActionPerformed

    private void treeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_treeButtonActionPerformed
        useTreeTableLayout();
    }//GEN-LAST:event_treeButtonActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel filterLabel;
    private javax.swing.JTextField filterTextField;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JCheckBox regExpFilterCheckBox;
    private javax.swing.JButton selectAllButton;
    private javax.swing.JButton selectInvertButton;
    private javax.swing.JLabel selectLabel;
    private javax.swing.JToggleButton tableButton;
    private javax.swing.JPanel topPanel;
    private javax.swing.JToggleButton treeButton;
    private org.openide.explorer.view.OutlineView view;
    // End of variables declaration//GEN-END:variables

    private List<Node> getAllNodes() {
        final boolean onlyFiles = selectOnlyFiles();

        Predicate<Node> nameFilter = (node) -> {
            return doFilter(node.getDisplayName());
        };
        Predicate<Node> tabOnlyFilter = (node) -> {
            return !onlyFiles || node instanceof TabNode;
        };
        Predicate<Node> filter = (node) -> {
            return nameFilter.test(node) && tabOnlyFilter.test(node);
        };
        return getAllChildren(em.getRootContext().getChildren(), filter);
    }

    private boolean selectOnlyFiles() {
        return true;
    }

    private List<Node> getAllChildren(Children children, Predicate<Node> filter) {
        List<Node> nodes = new ArrayList<>();
        children.snapshot().forEach((child) -> {

            if (filter.test(child)) {
                nodes.add(child);
            }

            if (!child.isLeaf()) {
                nodes.addAll(getAllChildren(child.getChildren(), filter));
            }
        });
        return nodes;
    }

    void writeProperties(java.util.Properties p) {
        LOG.fine("writeProperties");
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        p.setProperty("useTableViewType", String.valueOf(tableButton.isSelected()));
        p.setProperty("useRegExp", String.valueOf(regExpFilterCheckBox.isSelected()));
    }

    void readProperties(java.util.Properties p) {
        LOG.fine("readProperties");
        String version = p.getProperty("version");
        Boolean useTableViewType = Boolean.valueOf(p.getProperty("useTableViewType", "false"));
        Boolean useRegExp = Boolean.valueOf(p.getProperty("useRegExp", "false"));
        tableButton.setSelected(useTableViewType);
        regExpFilterCheckBox.setSelected(useRegExp);
    }

    private void validateNodeSelection(Node[] selection) {
        boolean tabIsSeleccted = Arrays.asList(selection).stream().anyMatch((node) -> node instanceof TabNode);
        boolean projIsSeleccted = Arrays.asList(selection).stream().anyMatch((node) -> node instanceof ProjNode);
        if (tabIsSeleccted && projIsSeleccted) {
            final TabNode[] onlyTabNodes
                    = Arrays.asList(selection).stream()
                            .filter((node) -> node instanceof TabNode)
                            .toArray(TabNode[]::new);
            SwingUtilities.invokeLater(() -> {
                try {
                    em.setSelectedNodes(onlyTabNodes);
                } catch (PropertyVetoException ex) {
                    Exceptions.printStackTrace(ex);
                }
            });
        }
    }

    private class NameQuickFilter implements QuickFilter {

        @Override
        public boolean accept(Object aValue) {
            boolean visible = true;
            if (hasFilter()) {
                if (aValue instanceof TabNode) {
                    TabNode taskNode = (TabNode) aValue;
                    visible = doFilter(taskNode.getDisplayName());
                } else if (aValue instanceof ProjNode) {
                    ProjNode pn = (ProjNode) aValue;
                    Children children = pn.getChildren();
                    for (Node child : children.snapshot()) {
                        if (child instanceof TabNode) {
                            if (visible = doFilter(child.getDisplayName())) {
                                break; // one visible tab child will be do
                            }
                        }
                    }
                }
            }
            return visible;
        }
    }

    private class ExpandChildNodeAdapter extends NodeAdapter {

        @Override
        public void childrenAdded(NodeMemberEvent ev) {
            LOG.fine("on addNodeListener.childrenAdded");
            if (ev.isAddEvent()) {
                for (Node projNode : ev.getSnapshot()) {
                    view.expandNode(projNode);
                }
            }
        }
    }
}
