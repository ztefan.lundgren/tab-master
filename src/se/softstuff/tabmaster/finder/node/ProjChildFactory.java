package se.softstuff.tabmaster.finder.node;

import se.softstuff.tabmaster.finder.Tab;
import se.softstuff.tabmaster.finder.Proj;
import java.beans.IntrospectionException;
import java.util.List;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;

public class ProjChildFactory extends ChildFactory<Tab> {

    private final Proj parent;

    public ProjChildFactory(Proj owner) {
        super();
        this.parent = owner;
    }

    @Override
    protected boolean createKeys(List<Tab> toPopulate) {
        toPopulate.addAll(parent.getTabs());
        return true;
    }

    @Override
    protected Node createNodeForKey(Tab tab) {
        TabNode node = null;
        try {
            node = new TabNode(tab);
        } catch (IntrospectionException ex) {
            Exceptions.printStackTrace(ex);
        }
        return node;
    }

}
