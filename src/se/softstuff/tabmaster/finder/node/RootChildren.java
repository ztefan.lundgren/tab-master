package se.softstuff.tabmaster.finder.node;

import se.softstuff.tabmaster.finder.Proj;
import java.beans.IntrospectionException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.netbeans.api.project.Project;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.windows.TopComponent;
import se.softstuff.tabmaster.EditorUtils;

public class RootChildren extends Children.Keys<Proj> {

    private final java.util.Map<Project, Collection<TopComponent>> editorTabs;

    public RootChildren() {
        editorTabs = EditorUtils.getEditorProjectTabs();
        if (editorTabs != null) {
            List<Proj> projs = new ArrayList<>();
            editorTabs.forEach((proj, tabs) -> {
                projs.add(new Proj(proj, tabs));
            });
            setKeys(projs);
        }
    }

    @Override
    protected Node[] createNodes(Proj proj) {
        try {
            return new Node[]{new ProjNode(proj)};
        } catch (IntrospectionException ex) {
            Exceptions.printStackTrace(ex);
            return null;
        }
    }

}
