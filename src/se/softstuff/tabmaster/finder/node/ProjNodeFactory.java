package se.softstuff.tabmaster.finder.node;

import se.softstuff.tabmaster.finder.Proj;
import java.beans.IntrospectionException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.netbeans.api.project.Project;
import org.openide.nodes.ChildFactory;
import org.openide.nodes.Node;
import org.openide.windows.TopComponent;
import se.softstuff.tabmaster.EditorUtils;

public class ProjNodeFactory extends ChildFactory<Proj> {

    private final Logger log = Logger.getLogger(getClass().getName());

    private Map<Project, Collection<TopComponent>> editorTabs;

    private boolean addProjectNode;

    public ProjNodeFactory() {
        super();
    }

    public boolean isAddProjectNode() {
        return addProjectNode;
    }

    public void setAddProjectNode(boolean addProjectNode) {
        this.addProjectNode = addProjectNode;
        refresh();
    }

    @Override
    protected boolean createKeys(final List<Proj> toPopulate) {
        if (editorTabs != null) {
            editorTabs.forEach((proj, tabs) -> {
                toPopulate.add(new Proj(proj, tabs));
            });
            log.fine("createKeys toPopulate: " + toPopulate.size());
            return true;
        }
        log.warning("createKeys, editorTabs is not set yet");
        return true;
    }

    @Override
    protected Node[] createNodesForKey(Proj proj) {

        if (addProjectNode) {
            try {
                return new Node[]{new ProjNode(proj)};
            } catch (IntrospectionException ex) {
                throw new RuntimeException("Failed to create node", ex);
            }
        } else {
            return proj.getTabs().stream()
                    .map((tab) -> {
                        try {
                            return new TabNode(tab);
                        } catch (IntrospectionException ex) {
                            throw new RuntimeException("Failed to create node", ex);
                        }
                    })
                    .toArray(TabNode[]::new);
        }
    }

    public void refresh() {
        log.fine("refresh " + SwingUtilities.isEventDispatchThread());

        if (!SwingUtilities.isEventDispatchThread()) {
            log.warning("Need to update nodes on EDT");
            return;
        }
        editorTabs = EditorUtils.getEditorProjectTabs();
        log.fine("call refresh");
        refresh(true);
    }
}
