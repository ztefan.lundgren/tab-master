package se.softstuff.tabmaster.finder.node;

import se.softstuff.tabmaster.finder.Tab;
import java.awt.Image;
import java.beans.IntrospectionException;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.logging.Logger;
import javax.swing.Action;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.api.debugger.Properties;
import org.openide.nodes.BeanNode;
import org.openide.nodes.Children;
import org.openide.nodes.NodeAdapter;
import org.openide.nodes.NodeEvent;
import org.openide.util.Utilities;
import org.openide.util.WeakListeners;
import org.openide.util.lookup.Lookups;
import org.openide.windows.TopComponent;
import se.softstuff.tabmaster.EditorUtils;

public class TabNode extends BeanNode<Tab> implements PropertyChangeListener {

    private static final Logger LOG = Logger.getLogger(TabNode.class.getName());

    public TabNode(final Tab tab) throws IntrospectionException {
        super(tab, Children.LEAF, Lookups.singleton(tab));
        final TopComponent tc = tab.getTc();
        super.setDisplayName(tab.getFile());
        super.setShortDescription(tc.getName());

        tc.addPropertyChangeListener(WeakListeners.propertyChange((TabNode) this, tc));

        final Properties debugger = Properties.getDefault().getProperties("debugger").getProperties(DebuggerManager.PROP_BREAKPOINTS);
        debugger.addPropertyChangeListener(WeakListeners.propertyChange((TabNode) this, debugger));

        addNodeListener(new NodeAdapter() {

            @Override
            public void nodeDestroyed(NodeEvent ev) {
                LOG.fine("nodeDestroyed TabNode " + getDisplayName());
                tc.removePropertyChangeListener(TabNode.this);
                debugger.removePropertyChangeListener(TabNode.this);
            }
        });

        boolean hasBreakpoint = EditorUtils.hasBreakpoint(tc);
        tab.setBreakpoint(hasBreakpoint);
        LOG.fine("Create TabNode " + getDisplayName());
    }

    @Override
    public Action[] getActions(boolean context) {
        return Utilities.actionsForPath("Tab/Actions").toArray(new Action[0]);
    }

    @Override
    public Action getPreferredAction() {
        return Utilities.actionsForPath("Tab/Actions").toArray(new Action[0])[0];
    }

    public Tab getTab() {
        return getBean();
    }

    @Override
    public Image getIcon(int type) {
        return getTab().getTc().getIcon();
    }

    @Override
    public void propertyChange(PropertyChangeEvent evt) {
        if (evt.getPropertyName().equals(EditorUtils.TM_LAST_ACCESS)) {
            LOG.fine("Node " + getName() + " changed, got a new lastAccess value " + getTab().getLastAccessStr());
            firePropertyChange("lastAccessStr", null, getTab().getLastAccessStr());
        } else if (evt.getPropertyName().equals(EditorUtils.TM_FREQUENCY)) {
            LOG.fine("Node " + getName() + " changed, got a new frequency value " + getTab().getFrequencyStr());
            firePropertyChange("frequencyStr", null, getTab().getFrequencyStr());
        } else if (evt.getPropertyName().equals("displayName") || evt.getPropertyName().equals("htmlDisplayName")) {
            LOG.fine("Node " + getName() + " changed, got a new editedStr value " + getTab().getEditedStr());
            firePropertyChange("editedStr", null, getTab().getEditedStr());
        } else if (evt.getPropertyName().matches("jpda")) {

            boolean hadBreakpoint = getTab().hasBreakpoint();
            boolean hasBreakpoint = EditorUtils.hasBreakpoint(getTab().getTc());
            if (hadBreakpoint != hasBreakpoint) {
                getTab().setBreakpoint(hasBreakpoint);
                LOG.fine("Node " + getName() + " changed, got a new breakpoint value " + getTab().getBreakpointStr());
                firePropertyChange("breakpoint", null, getTab().getBreakpointStr());
            }
        }
    }

}
