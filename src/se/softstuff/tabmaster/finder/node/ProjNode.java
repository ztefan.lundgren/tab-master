package se.softstuff.tabmaster.finder.node;

import se.softstuff.tabmaster.finder.Proj;
import java.beans.IntrospectionException;
import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.util.Utilities;
import org.openide.util.lookup.Lookups;

public class ProjNode extends AbstractNode {

    public ProjNode(Proj proj) throws IntrospectionException {
        super(Children.create(new ProjChildFactory(proj), true), Lookups.singleton(proj));
        super.setDisplayName(proj.getName());
        super.setShortDescription(proj.getName());
    }

    private Proj getProj() {
        return getLookup().lookup(Proj.class);
    }

    @Override
    public Action[] getActions(boolean context) {
        return Utilities.actionsForPath("TabProj/Actions").toArray(new Action[0]);
    }
}
