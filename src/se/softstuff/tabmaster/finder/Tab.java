package se.softstuff.tabmaster.finder;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.netbeans.api.project.Project;
import org.openide.filesystems.FileObject;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import se.softstuff.tabmaster.EditorUtils;

public class Tab {

    private static final String ATTRIBUTE_IS_MODIFIED = "ProvidedExtensions.VCSIsModified";
    public static final String PROP_FILE = "file";
    public static final String PROP_TC = "tc";
    public static final String PROP_CONTEXT = "context";
    public static final String PROP_EDITED = "edited";

    private String context;
    private TopComponent tc;

    private Project project;
    private String projectName;
    private boolean breakpoint;

    public Tab(Project project, TopComponent tc) {
        FileObject primaryFileFor = EditorUtils.getPrimaryFileFor(tc);
        this.context = primaryFileFor != null ? primaryFileFor.getMIMEType() : "n/a";
        this.tc = tc;
        this.project = project;
        this.projectName = project != null ? project.getProjectDirectory().getName() : "";
    }

    public TopComponent getTc() {
        return tc;
    }

    public String getContext() {
        return context;
    }

    public boolean isEdited() {
        return Boolean.TRUE.equals(EditorUtils.getPrimaryFileFor(tc).getAttribute(ATTRIBUTE_IS_MODIFIED))
                || tc.getName().contains("*")
                || tc.getHtmlDisplayName() != null && (tc.getHtmlDisplayName().contains("<b>") || tc.getHtmlDisplayName().contains("font"));
    }

    public String getEditedStr() {
        return NbBundle.getMessage(getClass(), isEdited() + "");
    }

    public Optional<LocalDateTime> getLastAccess() {
        return Optional.ofNullable((LocalDateTime) tc.getClientProperty(EditorUtils.TM_LAST_ACCESS));
    }
    
    public String getLastAccessStr() {
        return getLastAccess().isPresent() ? format(getLastAccess().get()) : "";
    }
    
    public Optional<Integer> getFrequency() {
        return Optional.ofNullable((Integer) tc.getClientProperty(EditorUtils.TM_FREQUENCY));
    }
                     
    public String getFrequencyStr() {
        return getFrequency().isPresent() ? getFrequency().get().toString() : "";
    }
    
    static String format(LocalDateTime time){
        return time.format(DateTimeFormatter.ISO_DATE_TIME);
    }

    public String getTimeSinceLastAccess() {
        Optional<LocalDateTime> lastAccess = getLastAccess();
        if (!lastAccess.isPresent()) {
            return NbBundle.getMessage(getClass(), "Never");
        }
        LocalDateTime now = LocalDateTime.now();
        Duration duration = Duration.between(lastAccess.get(), now);
        String result = DurationFormatUtils.formatDuration(duration.toMillis(), "d 'days' H 'hours' m 'minutes'", false);
        return result;

    }

    public String getFile() {
        return tc.getDisplayName() != null ? tc.getHtmlDisplayName() : tc.getName();
    }

    public Project getProject() {
        return project;
    }

    public String getProjectName() {
        return projectName;
    }

    public boolean getBreakpoint() {
        return breakpoint;
    }

    public boolean hasBreakpoint() {
        return breakpoint;
    }

    public String getBreakpointStr() {
        return NbBundle.getMessage(getClass(), hasBreakpoint() + "");
    }

    public void setBreakpoint(boolean breakpoint) {
        this.breakpoint = breakpoint;
    }

}
