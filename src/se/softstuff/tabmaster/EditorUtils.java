package se.softstuff.tabmaster;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.api.debugger.Properties;
import org.netbeans.api.debugger.jpda.LineBreakpoint;
import org.netbeans.api.project.FileOwnerQuery;
import org.netbeans.api.project.Project;
import org.netbeans.api.project.ui.OpenProjects;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.URLMapper;
import org.openide.loaders.DataNode;
import org.openide.loaders.DataObject;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.windows.Mode;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;

public class EditorUtils {

    public static final String TM_LAST_ACCESS = "tab-manager_last_access";
    public static final String TM_FREQUENCY = "tab-manager_frequency";

    private static final Logger LOG = Logger.getLogger(EditorUtils.class.getName());

    private EditorUtils() {
    }

    protected static Node getPrimaryNode(TopComponent tc) {
        Node[] nn = tc.getActivatedNodes();
        if (nn == null) {
            return null;
        }
        if (nn.length > 0) {
            Node n = nn[0];
            return n;
        } else {
            return null;
        }
    }

    static FileObject getFileObject(Node n) {
        try {
            if (n instanceof DataNode) {
                DataNode dn = (DataNode) n;
                DataObject o;
                FileObject fo;
                o = dn.getDataObject();
                if (o == null) {
                    return null;
                } else {
                    fo = o.getPrimaryFile();
                }
                return fo;
            } else {
                return null;
            }
        } catch (NullPointerException ex) {
            return null;
        }
    }

    public static FileObject getPrimaryFileFor(TopComponent tc) {
        Node[] nodes = tc.getActivatedNodes();
        if (nodes == null) {
            return null;
        }
        for (Node node : nodes) {
            DataObject obj = node.getCookie(DataObject.class);
            if (obj != null) {
                FileObject file = obj.getPrimaryFile();
                if (file != null) {
                    return file;
                }
            }
        }
        return null;
    }

    public static Optional<TopComponent> findTopComponent(DataObject dataObject) {
        return TopComponent.getRegistry().getOpened().stream()
                .filter((tc) -> WindowManager.getDefault().isEditorTopComponent(tc))
                .filter((tc) -> tc.getLookup().lookupAll(DataObject.class).contains(dataObject))
                .findFirst();

    }

    public static Optional<TopComponent> findTopComponent(String url) {
        return TopComponent.getRegistry().getOpened().stream()
                .filter((tc) -> WindowManager.getDefault().isEditorTopComponent(tc))
                .filter((tc) -> {
                    boolean anyMatch = tc.getLookup().lookupAll(DataObject.class).stream()
                            .anyMatch(data -> url.equalsIgnoreCase("file:/" + data.getPrimaryFile().getPath()));
                    return anyMatch;
                })
                .findFirst();

    }

    static String getLabelFor(TopComponent tc) {
        String dname = tc.getDisplayName();
        if (dname != null) {
            return dname;
        } else {
            return tc.getName();
        }
    }

    /**
     * taken from WhichProjectAction of which project, thanks.
     */
    public static Project getProjectFor(TopComponent tc) {

        Node[] nodes = tc.getActivatedNodes();
        if (nodes == null) {
            return null;
        }
        for (Node node : nodes) {
            DataObject obj = node.getCookie(DataObject.class);
            if (obj != null) {
                FileObject file = obj.getPrimaryFile();
                Project p = FileOwnerQuery.getOwner(file);
                if (p != null) {
                    return p;
                }
            }
        }
        return null;
    }

    public static Collection<TopComponent> getProjectsTabs(Collection<? extends Project> selectedProjects, boolean other) {
        Collection<TopComponent> openedTopComponents = getEditorTabs();
        return openedTopComponents.stream()
                .filter((project) -> selectedProjects.contains(EditorUtils.getProjectFor(project)) == !other)
                .collect(Collectors.toSet());
    }

    public static Collection<TopComponent> getEditorTabs() {
        LOG.fine("getEditorTabs begin");
        List<TopComponent> result = new ArrayList<>();
        final WindowManager wm = WindowManager.getDefault();
        for (Mode mode : wm.getModes()) {
            if (wm.isEditorMode(mode)) {
                TopComponent[] openedTopComponents = wm.getOpenedTopComponents(mode);
                LOG.log(Level.FINE, "getEditorTabs begin: {0} opened", openedTopComponents.length);
                result.addAll(Arrays.asList(openedTopComponents));
            }
        }
        result = result.stream().filter(tc -> tc.getName() != null && !tc.getName().equals("Start Page")).collect(Collectors.toList());
        LOG.fine("getEditorTabs done");
        return result;
    }

    public static void waitForProjectToLoad() throws InterruptedException, ExecutionException, TimeoutException {
        LOG.fine("börja vänta på att project laddat färdigt");
        Future<Project[]> openProjects = OpenProjects.getDefault().openProjects();
        LOG.log(Level.FINE, "fick en future {0}", openProjects.isDone());
        Project[] projects = openProjects.get(120000, TimeUnit.MILLISECONDS);
        LOG.log(Level.FINE, "all project is loaded, {0} open projects", projects.length);
    }

//    private final static Comparator<Project> nullSafeProjectComparator = Comparator.nullsFirst(Comparator.naturalOrder()); 
    private static final Comparator<Project> projectComparator = new Comparator<Project>() {
        @Override
        public int compare(Project p1, Project p2) {
            if (p1 == null && p2 == null) {
                return 0;
            }
            if (p1 == null) {
                return 1;
            }
            if (p2 == null) {
                return -1;
            }
            return p1.getProjectDirectory().getName().compareTo(p2.getProjectDirectory().getName());
        }

    };

    public static Map<Project, Collection<TopComponent>> getEditorProjectTabs() {
        Comparator<? super TopComponent> tcComp = (tcA, tcB) -> tcA.getName().compareTo(tcB.getName());

        SortedMap<Project, SortedSet<TopComponent>> projToTCMap = new TreeMap<>(projectComparator);
        getEditorTabs().forEach((tc) -> {
            if (tc.getName() != null) {
                Project proj = getProjectFor(tc);
                projToTCMap.putIfAbsent(proj, new TreeSet<>(tcComp));
                projToTCMap.get(proj).add(tc);
            }
        });
        return Collections.unmodifiableMap(projToTCMap);
    }

    public static boolean hasBreakpoint(TopComponent tc) {
        FileObject tcFile = EditorUtils.getPrimaryFileFor(tc);
        return hasBreakpoint(tcFile);
    }

    public static boolean hasBreakpoint(FileObject tcFile) {
        if (tcFile == null) {
            return false;
        }
        Properties debugger = Properties.getDefault().getProperties("debugger");
        Properties propBreakP = debugger.getProperties(DebuggerManager.PROP_BREAKPOINTS);
        Breakpoint[] breakpoints = (Breakpoint[]) propBreakP.getArray("jpda", new Breakpoint[0]);
        return Arrays.asList(breakpoints).stream()
                .anyMatch(bp -> {
                    if (bp instanceof LineBreakpoint) {
                        LineBreakpoint lb = (LineBreakpoint) bp;
                        try {
                            URL url = new URL(lb.getURL());
                            FileObject bpFile = URLMapper.findFileObject(url);
                            return tcFile.equals(bpFile);
                        } catch (MalformedURLException ex) {
                            Exceptions.printStackTrace(ex);
                        }
                    }
                    return false;
                });
    }
}
