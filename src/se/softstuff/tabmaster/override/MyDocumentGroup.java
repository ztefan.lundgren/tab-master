package se.softstuff.tabmaster.override;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.Collator;
import org.openide.util.Exceptions;
public final class MyDocumentGroup implements Comparable<MyDocumentGroup> {

    final Object instance;

    MyDocumentGroup(Object instance) {
        this.instance = instance;
    }

    @Override
    public String toString() {
        return instance.toString();
    }

    public String getDisplayName() {
        return instance.toString();
    }

    public String getName() {
        try {
            Method method = instance.getClass().getDeclaredMethod("getName");
            method.setAccessible(true);
            String name = (String) method.invoke(instance);
            return name;
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchMethodException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
        }
        return null;
    }

    @Override
    public int compareTo(MyDocumentGroup o) {
        Collator collator = Collator.getInstance();
        return collator.compare(instance, o.instance);
    }

    @Override
    public int hashCode() {
        return instance.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        return instance.equals(obj);
    }
}
