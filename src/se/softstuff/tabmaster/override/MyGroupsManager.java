package se.softstuff.tabmaster.override;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.openide.util.Exceptions;
import org.openide.windows.WindowManager;

public class MyGroupsManager {

    private static MyGroupsManager theInstance;

    private static final Logger LOG = Logger.getLogger(MyGroupsManager.class.getName());

    public static MyGroupsManager getDefault() {
        synchronized (MyGroupsManager.class) {
            if (null == theInstance) {

                try {

                    ClassLoader classLoader = WindowManager.getDefault().getClass().getClassLoader();
                    Class<?> GroupsManagerClass = Class.forName("org.netbeans.core.windows.documentgroup.GroupsManager", true, classLoader);
                    Method getDefault = GroupsManagerClass.getMethod("getDefault");
                    Object instance = getDefault.invoke(null);
                    theInstance = new MyGroupsManager(instance);
                } catch (Exception ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
            return theInstance;
        }
    }

    private final Object instance;

    private MyGroupsManager(Object instance) {
        this.instance = instance;
    }

    public void addGroup(String displayName) {
        try {
            Method method = instance.getClass().getDeclaredMethod("addGroup", String.class);
            method.setAccessible(true);
            method.invoke(instance, displayName);
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
            throw new RuntimeException(ex);
        }
    }

    public void removeAllGroups() {
        try {
            Method method = instance.getClass().getDeclaredMethod("removeAllGroups");
            method.setAccessible(true);
            method.invoke(instance);
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
            throw new RuntimeException(ex);
        }
    }

    public MyDocumentGroup getCurrentGroup() {
        try {
            Method method = instance.getClass().getDeclaredMethod("getCurrentGroup");
            method.setAccessible(true);
            Object group = method.invoke(instance);
            return new MyDocumentGroup(group);
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
            throw new RuntimeException(ex);
        }
    }

    private MyDocumentGroup createGroup(String groupName) {
        try {
            Method method = instance.getClass().getDeclaredMethod("createGroup");
            method.setAccessible(true);
            Object group = method.invoke(instance, groupName);
            return new MyDocumentGroup(group);
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
            throw new RuntimeException(ex);
        }
    }

    public List<MyDocumentGroup> getGroups() {
        try {
            Method method = instance.getClass().getDeclaredMethod("getGroups");
            method.setAccessible(true);
            List<Object> grouplist = (List<Object>) method.invoke(instance);
            return grouplist.stream()
                    .map(group -> new MyDocumentGroup(group))
                    .collect(Collectors.toList());
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
            throw new RuntimeException(ex);
        }
    }

    public boolean openGroup(MyDocumentGroup group) {
        try {
            Method method = instance.getClass().getDeclaredMethod("openGroup",
                    Class.forName("org.netbeans.core.windows.documentgroup.DocumentGroupImpl",
                            false,
                            WindowManager.getDefault().getClass().getClassLoader()));
            method.setAccessible(true);
            Boolean open = (Boolean) method.invoke(instance, group.instance);
            return open;
        } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
            throw new RuntimeException(ex);
        }
    }

    public boolean closeGroup(MyDocumentGroup group) {
        try {
            Method method = instance.getClass().getDeclaredMethod("closeGroup", Object.class);
            method.setAccessible(true);
            Boolean close = (Boolean) method.invoke(instance, group.instance);
            return close;
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
            throw new RuntimeException(ex);
        }
    }

    public void removeGroup(MyDocumentGroup group) {
        try {
            Method method = instance.getClass().getDeclaredMethod("removeGroup", Object.class);
            method.setAccessible(true);
            method.invoke(instance, group.instance);
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
            throw new RuntimeException(ex);
        }
    }

    public boolean closeAllDocuments() {
        try {
            Method method = instance.getClass().getDeclaredMethod("closeAllDocuments");
            method.setAccessible(true);
            boolean cloased = (boolean) method.invoke(null);
            return cloased;
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
            throw new RuntimeException(ex);
        }
    }
}
