package se.softstuff.tabmaster.override;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import org.openide.windows.TopComponent;

public class MyWindowManager {

    private static MyWindowManager INSTANCE;

    private static final Logger LOG = Logger.getLogger(MyWindowManager.class.getName());

    public static MyWindowManager getDefault() {
        if (null == INSTANCE) {
            synchronized (MyWindowManager.class) {
                if (null == INSTANCE) {
                    try {
                        Object impl = org.openide.windows.WindowManager.getDefault();
                        INSTANCE = new MyWindowManager(impl);
                    } catch (Exception ex) {
                        Exceptions.printStackTrace(ex);
                    }
                }
            }            
        }
        return INSTANCE;
    }

    private final Object impl;

    private MyWindowManager(Object windowManager) {
        this.impl = windowManager;
    }

    public void newTabGroup(TopComponent tc) {
        try {
            Method method = impl.getClass().getDeclaredMethod("newTabGroup", TopComponent.class);
            method.setAccessible(true);
            method.invoke(impl, tc);
        } catch (NoSuchMethodException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Exceptions.printStackTrace(ex);
            throw new RuntimeException(ex);
        }
    }
}
