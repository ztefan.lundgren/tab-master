package se.softstuff.tabmaster.timeline;

import java.beans.IntrospectionException;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.Lookup;

public class TimelineRootNode extends AbstractNode {

    static class TimelineChildren extends Children.Keys<TabFile> {

        private final FilterModel filterModel;
        private final TimelineModel timeLineModel;

        public TimelineChildren(FilterModel filterModel, TimelineModel timeLineModel) {
            this.filterModel = filterModel;
            this.timeLineModel = timeLineModel;

            filterModel.addPropertyChangeListener((evt) -> {
                filterKeys(timeLineModel.getTabFiles());
            });

            timeLineModel.addPropertyChangeListener((evt) -> {
                if (evt.getPropertyName().equals(TimelineModel.PROP_ADDTABFILE)
                        || evt.getPropertyName().equals(TimelineModel.PROP_REMOVETABFILE)) {
                    filterKeys(timeLineModel.getTabFiles());
                }
            });

            filterKeys(timeLineModel.getTabFiles());
        }

        private void filterKeys(Collection<TabFile> files) {
            Set<TabFile> filterdSet = files.stream()
                    .filter(filterModel.getFileFilter())
                    .collect(Collectors.toSet());
            setKeys(filterdSet);
        }

        @Override
        protected Node[] createNodes(TabFile key) {
            try {
                return new Node[]{new TabFileNode(key)};
            } catch (IntrospectionException ex) {
                Exceptions.printStackTrace(ex);
                return null;
            }
        }
    }

    public TimelineRootNode(FilterModel filterModel, TimelineModel timeLineModel) {
        super(new TimelineChildren(filterModel, timeLineModel), Lookup.EMPTY);
    }

}
