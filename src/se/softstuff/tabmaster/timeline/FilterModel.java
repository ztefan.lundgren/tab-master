package se.softstuff.tabmaster.timeline;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.function.Predicate;
import java.util.logging.Logger;

public class FilterModel {

    public static final String PROP_FILTER = "filter";
    public static final String PROP_REGEXP = "regexp";
    public static final String PROP_BREAKPOINT = "breakpoint";
    public static final String PROP_EDIT = "edit";
    public static final String PROP_DEBUGGER = "debugger";

    private String filter;

    private boolean regexp = false;

    private boolean breakpoint = false;

    private boolean edit = false;

    private boolean debugger = false;

    public boolean isDebugger() {
        return debugger;
    }

    public void setDebugger(boolean debugger) {
        boolean oldDebugger = this.debugger;
        this.debugger = debugger;
        propertyChangeSupport.firePropertyChange(PROP_DEBUGGER, oldDebugger, debugger);
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        boolean oldEdit = this.edit;
        this.edit = edit;
        propertyChangeSupport.firePropertyChange(PROP_EDIT, oldEdit, edit);
    }

    public boolean isBreakpoint() {
        return breakpoint;
    }

    public void setBreakpoint(boolean breakpoint) {
        boolean oldBreakpoint = this.breakpoint;
        this.breakpoint = breakpoint;
        propertyChangeSupport.firePropertyChange(PROP_BREAKPOINT, oldBreakpoint, breakpoint);
    }

    public boolean isRegexp() {
        return regexp;
    }

    public void setRegexp(boolean regexp) {
        boolean oldRegexp = this.regexp;
        this.regexp = regexp;
        propertyChangeSupport.firePropertyChange(PROP_REGEXP, oldRegexp, regexp);
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        String oldFilter = this.filter;
        this.filter = filter;
        propertyChangeSupport.firePropertyChange(PROP_FILTER, oldFilter, filter);
    }

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    Predicate<TabFile> getFileFilter() {
        return (tf) -> isFileVisible(tf);
    }

    private boolean isFileVisible(TabFile tf) {
        try {
            if (breakpoint && !tf.isBreakpoint()) {
                return false;
            }
            if (debugger && !tf.isDebugger()) {
                return false;
            }
            if (edit && !tf.isEdit()) {
                return false;
            }
            if (filter != null && !filter.trim().isEmpty() && tf.getFile() != null) {
                if (regexp) {
                    return tf.getDisplayName().matches(filter);
                } else {
                    return tf.getDisplayName().toLowerCase().contains(filter.toLowerCase());
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(getClass().getName()).warning(ex.getMessage());
        }
        return true;
    }

}
