/*
 * Copyright 2017 Stefan Lundgren.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.softstuff.tabmaster.timeline;

import java.awt.Component;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableCellRenderer;
import org.netbeans.api.settings.ConvertAsProperties;
import org.netbeans.swing.etable.ETableColumn;
import org.netbeans.swing.etable.ETableColumnModel;
import org.netbeans.swing.outline.Outline;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.explorer.ExplorerManager;
import org.openide.explorer.ExplorerUtils;
import org.openide.nodes.Node;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;

/**
 * Top component which displays something.
 */
@ConvertAsProperties(
        dtd = "-//se.softstuff.tabmaster.timeline//TimeLine//EN",
        autostore = false
)
@TopComponent.Description(
        preferredID = "TimelineTopComponent",
        iconBase = "se/softstuff/tabmaster/timeline/time_go.png",
        persistenceType = TopComponent.PERSISTENCE_ALWAYS
)
@TopComponent.Registration(mode = "rightSlidingSide", openAtStartup = true)
@ActionID(category = "Window", id = "se.softstuff.tabmaster.timeline.TimelineTopComponent")
@ActionReference(path = "Menu/Window" , position = 996 )
@TopComponent.OpenActionRegistration(
        displayName = "#CTL_TimelineAction",
        preferredID = "TimelineTopComponent"
)
public final class TimelineTopComponent extends TopComponent implements ExplorerManager.Provider {

    private static final Logger LOG = Logger.getLogger(TimelineTopComponent.class.getName());

    private final ExplorerManager em;

    public TimelineTopComponent() {
        initComponents();
        setName(NbBundle.getMessage(getClass(), "CTL_TimelineTopComponent"));
        setToolTipText(NbBundle.getMessage(getClass(), "HINT_TimelineTopComponent"));

        view.setPropertyColumns(
                "timestampStr", NbBundle.getMessage(getClass(), "Viewed"),
                //      "view", "",
                "editIcon", "",
                "breakpointIcon", "",
                "debuggerIcon", ""
        );
        //view.setPropertyColumnDescription("view", NbBundle.getMessage(getClass(), "View"));
        view.setPropertyColumnDescription("editIcon", NbBundle.getMessage(getClass(), "Edited"));
        view.setPropertyColumnDescription("breakpointIcon", NbBundle.getMessage(getClass(), "Breakpoint"));
        view.setPropertyColumnDescription("debuggerIcon", NbBundle.getMessage(getClass(), "InDebugger"));

        //ImageRenderer viewRenderer = new ImageRenderer(NbBundle.getMessage(getClass(), "View"), TabFile.IMG_VIEW);
        ImageRenderer breakpointRenderer = new ImageRenderer(NbBundle.getMessage(getClass(), "Breakpoint"));
        ImageRenderer editRenderer = new ImageRenderer(NbBundle.getMessage(getClass(), "Edited"));
        ImageRenderer debuggerRenderer = new ImageRenderer(NbBundle.getMessage(getClass(), "InDebugger"));

        Outline outline = view.getOutline();
        outline.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        outline.setRootVisible(false);
        outline.setColumnSorted(1, false, 1);
        outline.getColumnModel().getColumn(0).setPreferredWidth(100);
        outline.getColumnModel().getColumn(0).sizeWidthToFit();
        outline.getColumnModel().getColumn(1).setPreferredWidth(50);
        outline.getColumnModel().getColumn(1).setMinWidth(100);
        int cellWidth = 30;
        for (int i = 2; i < 5; i++) {
            outline.getColumnModel().getColumn(i).setPreferredWidth(cellWidth);
            outline.getColumnModel().getColumn(i).setResizable(false);
            outline.getColumnModel().getColumn(i).setMaxWidth(cellWidth);
            outline.getColumnModel().getColumn(i).setMinWidth(cellWidth);
        }
        //outline.getColumnModel().getColumn(2).setCellRenderer(viewRenderer);
        outline.getColumnModel().getColumn(2).setCellRenderer(editRenderer);
        outline.getColumnModel().getColumn(3).setCellRenderer(breakpointRenderer);
        outline.getColumnModel().getColumn(4).setCellRenderer(debuggerRenderer);

        ((ETableColumn) outline.getColumnModel().getColumn(1)).setCustomIcon(TabFile.IMG_VIEW);
        ((ETableColumn) outline.getColumnModel().getColumn(2)).setCustomIcon(TabFile.IMG_EDIT);
        ((ETableColumn) outline.getColumnModel().getColumn(3)).setCustomIcon(TabFile.IMG_BREAKPOINT);
        ((ETableColumn) outline.getColumnModel().getColumn(4)).setCustomIcon(TabFile.IMG_DEBUGGER);

        outline.setModel(outline.getModel());

        em = new ExplorerManager();
        timeLineModel.setExplorerManager(em);

        associateLookup(ExplorerUtils.createLookup(em, getActionMap()));
        em.setRootContext(new TimelineRootNode(filterModel, timeLineModel));

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        filterModel = new se.softstuff.tabmaster.timeline.FilterModel();
        timeLineModel = new se.softstuff.tabmaster.timeline.TimelineModel();
        filterPanel = new javax.swing.JPanel();
        filterTextField = new javax.swing.JTextField();
        filterLabel = new javax.swing.JLabel();
        regExpFilterCheckBox = new javax.swing.JCheckBox();
        jPanel2 = new javax.swing.JPanel();
        editCheckBox = new javax.swing.JCheckBox();
        brCheckBox = new javax.swing.JCheckBox();
        dsCheckBox2 = new javax.swing.JCheckBox();
        view = new org.openide.explorer.view.OutlineView();
        limitComboBox = new javax.swing.JComboBox<>();
        limitLabel = new javax.swing.JLabel();

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, filterModel, org.jdesktop.beansbinding.ELProperty.create("${filter}"), filterTextField, org.jdesktop.beansbinding.BeanProperty.create("text"));
        bindingGroup.addBinding(binding);

        org.openide.awt.Mnemonics.setLocalizedText(filterLabel, org.openide.util.NbBundle.getMessage(TimelineTopComponent.class, "TimelineTopComponent.filterLabel.text")); // NOI18N

        org.openide.awt.Mnemonics.setLocalizedText(regExpFilterCheckBox, org.openide.util.NbBundle.getMessage(TimelineTopComponent.class, "TimelineTopComponent.regExpFilterCheckBox.text")); // NOI18N
        regExpFilterCheckBox.setToolTipText(org.openide.util.NbBundle.getMessage(TimelineTopComponent.class, "TimelineTopComponent.regExpFilterCheckBox.toolTipText")); // NOI18N

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, filterModel, org.jdesktop.beansbinding.ELProperty.create("${regexp}"), regExpFilterCheckBox, org.jdesktop.beansbinding.BeanProperty.create("selected"));
        bindingGroup.addBinding(binding);

        jPanel2.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.LEFT));

        org.openide.awt.Mnemonics.setLocalizedText(editCheckBox, org.openide.util.NbBundle.getMessage(TimelineTopComponent.class, "TimelineTopComponent.editCheckBox.text")); // NOI18N

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, filterModel, org.jdesktop.beansbinding.ELProperty.create("${edit}"), editCheckBox, org.jdesktop.beansbinding.BeanProperty.create("selected"));
        bindingGroup.addBinding(binding);

        jPanel2.add(editCheckBox);

        org.openide.awt.Mnemonics.setLocalizedText(brCheckBox, org.openide.util.NbBundle.getMessage(TimelineTopComponent.class, "TimelineTopComponent.brCheckBox.text")); // NOI18N

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, filterModel, org.jdesktop.beansbinding.ELProperty.create("${breakpoint}"), brCheckBox, org.jdesktop.beansbinding.BeanProperty.create("selected"));
        bindingGroup.addBinding(binding);

        jPanel2.add(brCheckBox);

        org.openide.awt.Mnemonics.setLocalizedText(dsCheckBox2, org.openide.util.NbBundle.getMessage(TimelineTopComponent.class, "TimelineTopComponent.dsCheckBox2.text")); // NOI18N

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, filterModel, org.jdesktop.beansbinding.ELProperty.create("${debugger}"), dsCheckBox2, org.jdesktop.beansbinding.BeanProperty.create("selected"));
        bindingGroup.addBinding(binding);

        jPanel2.add(dsCheckBox2);

        javax.swing.GroupLayout filterPanelLayout = new javax.swing.GroupLayout(filterPanel);
        filterPanel.setLayout(filterPanelLayout);
        filterPanelLayout.setHorizontalGroup(
            filterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(filterPanelLayout.createSequentialGroup()
                .addComponent(filterLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(filterTextField)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(regExpFilterCheckBox)
                .addContainerGap())
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        filterPanelLayout.setVerticalGroup(
            filterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(filterPanelLayout.createSequentialGroup()
                .addGroup(filterPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(filterLabel)
                    .addComponent(filterTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(regExpFilterCheckBox))
                .addGap(1, 1, 1)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        limitComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "10", "50", "100", "200", "1000" }));
        limitComboBox.setSelectedIndex(1);

        binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, timeLineModel, org.jdesktop.beansbinding.ELProperty.create("${listSizeLimit}"), limitComboBox, org.jdesktop.beansbinding.BeanProperty.create("selectedItem"));
        bindingGroup.addBinding(binding);

        org.openide.awt.Mnemonics.setLocalizedText(limitLabel, org.openide.util.NbBundle.getMessage(TimelineTopComponent.class, "TimelineTopComponent.limitLabel.text")); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(filterPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(view, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(limitLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(limitComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(filterPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(view, javax.swing.GroupLayout.DEFAULT_SIZE, 106, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(limitComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(limitLabel)))
        );

        bindingGroup.bind();
    }// </editor-fold>//GEN-END:initComponents

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JCheckBox brCheckBox;
    private javax.swing.JCheckBox dsCheckBox2;
    private javax.swing.JCheckBox editCheckBox;
    private javax.swing.JLabel filterLabel;
    private se.softstuff.tabmaster.timeline.FilterModel filterModel;
    private javax.swing.JPanel filterPanel;
    private javax.swing.JTextField filterTextField;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JComboBox<String> limitComboBox;
    private javax.swing.JLabel limitLabel;
    private javax.swing.JCheckBox regExpFilterCheckBox;
    private se.softstuff.tabmaster.timeline.TimelineModel timeLineModel;
    private org.openide.explorer.view.OutlineView view;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
    @Override
    public void componentOpened() {
        timeLineModel.activate();
    }

    @Override
    public void componentClosed() {
        timeLineModel.deactivate();
    }

    @Override
    public ExplorerManager getExplorerManager() {
        return em;
    }

    void writeProperties(java.util.Properties p) {
        // better to version settings since initial version as advocated at
        // http://wiki.apidesign.org/wiki/PropertyFiles
        p.setProperty("version", "1.0");
        // TODO store your settings

        Outline outline = view.getOutline();
        ETableColumnModel columnModel = (ETableColumnModel) outline.getColumnModel();
//        columnModel.writeSettings(p, "TabTimeline.view");
    }

    void readProperties(java.util.Properties p) {
        String version = p.getProperty("version");
        // TODO read your settings according to their version

        Outline outline = view.getOutline();
        ETableColumnModel columnModel = (ETableColumnModel) outline.getColumnModel();
//        columnModel.readSettings(p, "TabTimeline.view", outline);
    }

    private String getFilter() {
        return filterTextField.getText().trim();
    }

    private boolean hasFilter() {
        return getFilter() != null && !getFilter().isEmpty();
    }

    private boolean isFilterRegexp() {
        return regExpFilterCheckBox.isSelected();
    }

    private boolean doFilter(String fileName) {
        if (hasFilter()) {
            if (isFilterRegexp()) {
                try {
                    Pattern regexPattern = Pattern.compile(getFilter());
                    Matcher regex = regexPattern.matcher(fileName);
                    return regex.matches();
                } catch (java.util.regex.PatternSyntaxException ex) {
                    return true;
                }
            } else {
                return fileName.toLowerCase().contains(getFilter().toLowerCase());
            }
        }
        return true;
    }

    class ImageRenderer extends DefaultTableCellRenderer {

        JLabel lbl;

        public ImageRenderer(String tooltip) {
            lbl = new JLabel();
            lbl.setToolTipText(tooltip);
        }

        private ImageRenderer(String tooltip, ImageIcon icon) {
            this(tooltip);
            lbl.setIcon(icon);
        }

        @Override
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                boolean hasFocus, int row, int column) {
            try {
                if (value != null) {
                    Node.Property<?> node = (Node.Property<?>) value;
                    Object cellValue = node.getValue();
                    if (cellValue instanceof ImageIcon) {
                        lbl.setIcon((ImageIcon) cellValue);
                    }
                }
                lbl.setText("");
            } catch (IllegalAccessException ex) {
                Exceptions.printStackTrace(ex);
            } catch (InvocationTargetException ex) {
                Exceptions.printStackTrace(ex);
            }
            return lbl;
        }
    }
}
