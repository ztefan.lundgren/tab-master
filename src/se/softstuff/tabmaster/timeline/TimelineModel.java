package se.softstuff.tabmaster.timeline;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Collection;
import java.util.Deque;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.netbeans.api.debugger.ActionsManagerListener;
import org.netbeans.api.debugger.Breakpoint;
import org.netbeans.api.debugger.DebuggerEngine;
import org.netbeans.api.debugger.DebuggerManager;
import org.netbeans.api.debugger.DebuggerManagerAdapter;
import org.netbeans.api.debugger.DebuggerManagerListener;
import org.netbeans.api.debugger.Session;
import org.netbeans.api.debugger.jpda.LineBreakpoint;
import org.netbeans.spi.debugger.jpda.EditorContext;
import org.openide.explorer.ExplorerManager;
import org.openide.filesystems.FileObject;
import org.openide.loaders.DataObject;
import org.openide.windows.TopComponent;
import org.openide.windows.WindowManager;
import se.softstuff.tabmaster.EditorUtils;

public class TimelineModel {

    public static final String PROP_ADDTABFILE = "addTabFile";
    public static final String PROP_REMOVETABFILE = "removeTabFile";
    public static final String PROP_LISTSIZELIMIT = "listSizeLimit";

    private int listSizeLimit = 50;

    private final Deque<TabFile> timeline;
    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
    private final Logger log = Logger.getLogger(getClass().getName());
    private boolean isDebugging;

    private ExplorerManager explorerManager;

    public int getListSizeLimit() {
        return listSizeLimit;
    }

    public void setListSizeLimit(int listSizeLimit) {
        int oldListSizeLimit = this.listSizeLimit;
        this.listSizeLimit = listSizeLimit;
        propertyChangeSupport.firePropertyChange(PROP_LISTSIZELIMIT, oldListSizeLimit, listSizeLimit);

        verifyMaxSizeLimit();
    }

    private void removeTabFile(TabFile removeTabFile) {
        if (timeline.remove(removeTabFile)) {
            propertyChangeSupport.firePropertyChange(PROP_REMOVETABFILE, removeTabFile, null);
        }
    }

    private void addUrlTab(String url, boolean edit, boolean debugger) {
        Runnable task = () -> {
            Optional<TopComponent> optTc = EditorUtils.findTopComponent(url);
            if (optTc.isPresent()) {
                boolean breakpoint = checkIfDebuggerHitABreakpoint(url);
                addTabFile(new TabFile(optTc.get(), edit, debugger, breakpoint));
            }
        };
        if (SwingUtilities.isEventDispatchThread()) {
            task.run();
        } else {
            SwingUtilities.invokeLater(task);
        }
    }

    private void addTabFile(TabFile addTabFile) {
        TabFile lastTab = timeline.peekLast();
        if (lastTab != null && lastTab.getFile().equals(addTabFile.getFile())) {
            lastTab.setBreakpoint(lastTab.isBreakpoint() || addTabFile.isBreakpoint());
            lastTab.setDebugger(lastTab.isDebugger() || addTabFile.isDebugger());
            lastTab.setEdit(lastTab.isEdit() || addTabFile.isEdit());
            lastTab.setTimestamp(addTabFile.getTimestamp());
        } else {
            timeline.add(addTabFile);
            propertyChangeSupport.firePropertyChange(PROP_ADDTABFILE, null, addTabFile);

            verifyMaxSizeLimit();
        }

    }

    private void verifyMaxSizeLimit() {
        while (timeline.size() > listSizeLimit) {
            TabFile oldest = timeline.peekFirst();
            if (oldest != null) {
                removeTabFile(oldest);
            }
        }
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    public TimelineModel() {
        this.timeline = new ConcurrentLinkedDeque<>();
    }

    Queue<TabFile> getTabFiles() {
        return timeline;
    }

    void activate() {
        TopComponent.getRegistry().addPropertyChangeListener(topComponentListner);
        DataObject.getRegistry().addChangeListener(dataObjectListener);

        DebuggerManager.getDebuggerManager().addDebuggerListener(debuggerListener);
    }

    void deactivate() {
        TopComponent.getRegistry().removePropertyChangeListener(topComponentListner);
        DataObject.getRegistry().removeChangeListener(dataObjectListener);
    }

    private boolean checkIfDebuggerHitABreakpoint(TopComponent tc) {
        FileObject primaryFile = EditorUtils.getPrimaryFileFor(tc);
        if (primaryFile == null || !isDebugging) {
            return false;
        }
        String path = primaryFile.getPath();
        return checkIfDebuggerHitABreakpoint(path);
    }

    private boolean checkIfDebuggerHitABreakpoint(String path) {
        if (!path.startsWith("file:/")) {
            path = "file:/" + path;
        }
        List<? extends EditorContext> editorContexts = (List<? extends EditorContext>) DebuggerManager.getDebuggerManager().lookup(null, EditorContext.class);
        for (EditorContext editorContext : editorContexts) {
            String currentURL = editorContext.getCurrentURL();

            if (currentURL.equalsIgnoreCase(path)) {
                int currentLineNumber = editorContext.getCurrentLineNumber();
                log.fine(String.format("Yes we the debugger hit brakpoint %s at row %d", editorContext.getCurrentURL(), currentLineNumber));
                return findBreakpoint(currentURL, currentLineNumber) != null;
            }
        }
        return false;
    }

    public void setExplorerManager(ExplorerManager explorerManager) {
        this.explorerManager = explorerManager;
    }

    private final PropertyChangeListener topComponentListner = (PropertyChangeEvent evt) -> {
        if (evt.getPropertyName().equals("activated")) {
            TopComponent tc = (TopComponent) evt.getNewValue();

            if (WindowManager.getDefault().isEditorTopComponent(tc)) {
                FileObject primaryFile = EditorUtils.getPrimaryFileFor(tc);
                if (primaryFile == null) {
                    return;
                }
                log.fine("TC " + tc.getName() + " was opende");
                boolean edit = false;
                boolean debugger = isDebugging;
                boolean breakpoint = checkIfDebuggerHitABreakpoint(tc);
                addTabFile(new TabFile(tc, edit, debugger, breakpoint));
            }
        }
    };

    static LineBreakpoint findBreakpoint(String url, int lineNumber) {
        Breakpoint[] breakpoints = DebuggerManager.getDebuggerManager().getBreakpoints();
        for (int i = 0; i < breakpoints.length; i++) {
            if (!(breakpoints[i] instanceof LineBreakpoint)) {
                continue;
            }
            LineBreakpoint lb = (LineBreakpoint) breakpoints[i];
            if (!lb.getURL().equals(url)) {
                continue;
            }
            if (lineNumber == -1 || lb.getLineNumber() == lineNumber) {
                return lb;
            }
        }
        return null;
    }

    private final DebuggerManagerListener debuggerListener = new DebuggerManagerAdapter() {

        @Override
        public void sessionAdded(Session sn) {
            log.log(Level.FINE, () -> "debuggerListener.sessionAdded");
            super.sessionAdded(sn);
            isDebugging = true;
        }

        @Override
        public void sessionRemoved(Session sn) {
            log.log(Level.FINE, () -> "debuggerListener.sessionRemoved");
            super.sessionRemoved(sn);
            isDebugging = false;
        }

        @Override
        public void engineAdded(DebuggerEngine de) {
            log.log(Level.FINE, () -> "debuggerListener.engineAdded");
            super.engineAdded(de);
            de.getActionsManager().addActionsManagerListener(debuggerActionListener);
        }

        @Override
        public void engineRemoved(DebuggerEngine de) {
            log.log(Level.FINE, () -> "debuggerListener.engineRemoved");
            super.engineRemoved(de);
            de.getActionsManager().removeActionsManagerListener(debuggerActionListener);
        }

        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            log.log(Level.FINE, () -> String.format("debuggerListener %s -> new:%s old: %s%n", evt.getPropertyName(), evt.getNewValue(), evt.getOldValue()));
        }
    };

    private final ActionsManagerListener debuggerActionListener = new ActionsManagerListener() {
        @Override
        public void actionPerformed(Object o) {
            log.log(Level.FINE, () -> String.format("debuggerActionListener.actionPerformed( %s )", o));
        }

        @Override
        public void actionStateChanged(Object o, boolean bln) {
            if (o.equals("pause") && bln) {
                log.log(Level.FINE, () -> String.format("debuggerActionListener.actionStateChanged(%s, %s)", o, bln));

                List<? extends EditorContext> editorContexts = DebuggerManager.getDebuggerManager().lookup(null, EditorContext.class);
                Optional<? extends EditorContext> optEditor = editorContexts.stream()
                        .filter((ec -> !ec.getCurrentURL().isEmpty()))
                        .findFirst();
                if (optEditor.isPresent()) {
                    String currentURL = optEditor.get().getCurrentURL();
                    boolean edit = false;
                    boolean debugger = isDebugging;
                    addUrlTab(currentURL, edit, debugger);
                }
            }
        }
    };
    ChangeListener dataObjectListener = new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
            log.fine(String.format("DataObject e: %s", e.getSource().getClass()));
            log.fine(String.format("DataObject e.source: %s", e.getSource()));
            if (e.getSource() instanceof Collection) {
                Collection<DataObject> changes = (Set<DataObject>) e.getSource();
                Runnable task = () -> {
                    for (DataObject data : changes) {
                        Optional<TopComponent> optTc = EditorUtils.findTopComponent(data);
                        if (optTc.isPresent() && optTc.get() == TopComponent.getRegistry().getActivated()) {
                            boolean edit = data.isModified();
                            boolean debugger = isDebugging;
                            boolean breakpoint = false;
                            addTabFile(new TabFile(optTc.get(), edit, debugger, breakpoint));
                            break;
                        }
                    }
                };
                if (SwingUtilities.isEventDispatchThread()) {
                    task.run();
                } else {
                    SwingUtilities.invokeLater(task);
                }
            }
        }
    };

}
