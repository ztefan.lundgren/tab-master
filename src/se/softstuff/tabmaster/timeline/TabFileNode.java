package se.softstuff.tabmaster.timeline;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.IntrospectionException;
import java.io.File;
import java.util.logging.Logger;
import javax.swing.Action;
import org.netbeans.api.actions.Closable;
import org.netbeans.api.actions.Openable;
import org.openide.awt.Actions;
import org.openide.filesystems.FileObject;
import org.openide.filesystems.FileUtil;
import org.openide.loaders.DataObject;
import org.openide.loaders.DataObjectNotFoundException;
import org.openide.nodes.BeanNode;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle;
import se.softstuff.tabmaster.finder.node.TabNode;

public class TabFileNode extends BeanNode<TabFile> {

    private static final Logger LOG = Logger.getLogger(TabNode.class.getName());

    public TabFileNode(final TabFile tab) throws IntrospectionException {
        super(tab);
        super.setDisplayName(tab.getDisplayName());
        super.setShortDescription(tab.getDisplayName());
    }

    @Override
    public Image getIcon(int type) {
        return getBean().getIcon();
    }

    @Override
    public Action[] getActions(boolean context) {

        return new Action[]{openFileAction, closeFileAction};
    }

    @Override
    public Action getPreferredAction() {
        return openFileAction;
    }

    private final Action openFileAction = Actions.alwaysEnabled(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            LOG.fine("Open file " + getDisplayName());
            String filepath = getBean().getFile();
            File file = new File(filepath);
            if (file.exists()) {
                try {
                    FileObject fileObject = FileUtil.toFileObject(FileUtil.normalizeFile(file));
                    DataObject dob = DataObject.find(fileObject);
                    Openable openable = dob.getLookup().lookup(Openable.class);
                    if (openable != null) {
                        openable.open();
                    }
                } catch (DataObjectNotFoundException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
    }, NbBundle.getMessage(TabFileNode.class, "Open_file"), null, true);

    private final Action closeFileAction = Actions.alwaysEnabled(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            LOG.fine("Close file " + getDisplayName());
            String filepath = getBean().getFile();
            File file = new File(filepath);
            if (file.exists()) {
                try {
                    FileObject fileObject = FileUtil.toFileObject(FileUtil.normalizeFile(file));
                    DataObject dob = DataObject.find(fileObject);
                    Closable closable = dob.getLookup().lookup(Closable.class);
                    if (closable != null) {
                        closable.close();
                    }
                } catch (DataObjectNotFoundException ex) {
                    Exceptions.printStackTrace(ex);
                }
            }
        }
    }, NbBundle.getMessage(TabFileNode.class, "Close_file"), null, true);
}
