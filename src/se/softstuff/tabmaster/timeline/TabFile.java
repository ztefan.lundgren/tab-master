package se.softstuff.tabmaster.timeline;

import java.awt.Image;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.Objects;
import javax.swing.ImageIcon;
import org.openide.util.ImageUtilities;
import org.openide.util.NbBundle;
import org.openide.windows.TopComponent;
import se.softstuff.tabmaster.EditorUtils;

public class TabFile {

    public static final ImageIcon IMG_BREAKPOINT = ImageUtilities.loadImageIcon("se/softstuff/tabmaster/timeline/bullet_pink.png", false);
    public static final ImageIcon IMG_EDIT = ImageUtilities.loadImageIcon("se/softstuff/tabmaster/timeline/page_white_edit.png", false);
    public static final ImageIcon IMG_DEBUGGER = ImageUtilities.loadImageIcon("se/softstuff/tabmaster/timeline/control_play_green.png", false);
    public static final ImageIcon IMG_VIEW = ImageUtilities.loadImageIcon("se/softstuff/tabmaster/timeline/eye.png", false);
    public static final ImageIcon IMG_EMPTY = ImageUtilities.loadImageIcon("se/softstuff/tabmaster/timeline/empty.png", false);

    public static final String PROP_BREAKPOINT = "breakpoint";
    public static final String PROP_EDIT = "edit";
    public static final String PROP_DEBUGGER = "debugger";
    public static final String PROP_TIMESTAMP = "timestamp";
    public static final String PROP_DISPLAYNAME = "displayName";
    public static final String PROP_DISPLAYNAMEHTML = "displayNameHtml";

    private LocalDateTime timestamp;

    private boolean breakpoint = false;

    private boolean edit = false;

    private boolean debugger = false;

    private String displayName;

    private String htmlDisplayName;

    private Image icon;

    private final URL file;

    private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public TabFile(TopComponent tc, boolean edit, boolean debugger, boolean breakpoint) {
        this.timestamp = LocalDateTime.now();
        this.displayName = tc.getDisplayName() != null ? tc.getDisplayName() : tc.getName();
        this.htmlDisplayName = tc.getHtmlDisplayName();
        this.icon = tc.getIcon();
        this.file = EditorUtils.getPrimaryFileFor(tc).toURL();
        this.edit = edit;
        this.debugger = debugger;
        this.breakpoint = breakpoint;

    }

    public Image getIcon() {
        return icon;
    }

    public void setIcon(Image icon) {
        this.icon = icon;
    }

    public String getFile() {
        return file.getPath();
    }

    public String getHtmlDisplayName() {
        return htmlDisplayName;
    }

    public void setHtmlDisplayName(String htmlDisplayName) {
        String oldDisplayNameHtml = this.htmlDisplayName;
        this.htmlDisplayName = htmlDisplayName;
        propertyChangeSupport.firePropertyChange(PROP_DISPLAYNAMEHTML, oldDisplayNameHtml, htmlDisplayName);
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        String oldDisplayName = this.displayName;
        this.displayName = displayName;
        propertyChangeSupport.firePropertyChange(PROP_DISPLAYNAME, oldDisplayName, displayName);
    }

    public String getTimestampStr() {
        return timestamp.toString();
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        LocalDateTime oldTimestamp = this.timestamp;
        this.timestamp = timestamp;
        propertyChangeSupport.firePropertyChange(PROP_TIMESTAMP, oldTimestamp, timestamp);
    }

    public boolean isDebugger() {
        return debugger;
    }

    public String getDebuggerStr() {
        return NbBundle.getMessage(getClass(), debugger ? "Yes" : "No");
    }

    public ImageIcon getDebuggerIcon() {
        return isDebugger() ? IMG_DEBUGGER : IMG_EMPTY;
    }

    public void setDebugger(boolean debugger) {
        boolean oldDebugger = this.debugger;
        this.debugger = debugger;
        propertyChangeSupport.firePropertyChange(PROP_DEBUGGER, oldDebugger, debugger);
    }

    public boolean isEdit() {
        return edit;
    }

    public String getEditStr() {
        return NbBundle.getMessage(getClass(), edit ? "Yes" : "No");
    }

    public ImageIcon getEditIcon() {
        return isEdit() ? IMG_EDIT : IMG_EMPTY;
    }

    public void setEdit(boolean edit) {
        boolean oldEdit = this.edit;
        this.edit = edit;
        propertyChangeSupport.firePropertyChange(PROP_EDIT, oldEdit, edit);
    }

    public boolean isBreakpoint() {
        return breakpoint;
    }

    public String getBreakpointStr() {
        return NbBundle.getMessage(getClass(), breakpoint ? "Yes" : "No");
    }

    public ImageIcon getBreakpointIcon() {
        return isBreakpoint() ? IMG_BREAKPOINT : IMG_EMPTY;
    }

    public void setBreakpoint(boolean breakpoint) {
        boolean oldBreakpoint = this.breakpoint;
        this.breakpoint = breakpoint;
        propertyChangeSupport.firePropertyChange(PROP_BREAKPOINT, oldBreakpoint, breakpoint);
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.file);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TabFile other = (TabFile) obj;
        if (!Objects.equals(this.timestamp, other.timestamp)) {
            return false;
        }
        if (!Objects.equals(this.file, other.file)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TabFile{ displayName=" + displayName + " timestamp=" + timestamp + ", breakpoint=" + breakpoint + ", edit=" + edit + ", debugger=" + debugger + "}";
    }
}
